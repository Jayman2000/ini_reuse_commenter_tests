<!--SPDX-FileCopyrightText: 2021 Jason Yundt <swagfortress@gmail.com>
SPDX-License-Identifier: CC0-1.0

SPDX-FileCopyrightText: 2019 A.B <https://unix.stackexchange.com/users/251756/a-b>
SPDX-License-Identifier: CC-BY-SA-4.0
-->
# INI REUSE Commenter Tests
Tests for the [INI REUSE Commenter](https://gitlab.com/Jayman2000/ini_reuse_commenter).

## Copying
This repo should be [REUSE](https://reuse.software/) compliant. For copying information, install the [reuse tool](https://github.com/fsfe/reuse-tool), and run `reuse spdx`. This repo's submodule, `ini_reuse_commenter`, is also REUSE compliant. For copying information about it, `cd` to the `ini_reuse_commenter` directory, and run `reuse spdx`. This repo's submodule, `WAT-GDScript` is not REUSE compliant. For copying information about it, see its `LICENSE` file.

## Importing the project
<ol>
<li>Clone this repo:<br>
<code>$ git clone --recursive &lt;repository&gt;</code></li>

<li>If you didn't clone this repo with the --recursive flag, then the ini_reuse_commenter and the WAT-GDScript folders will be empty. To fix this, <code>cd</code> to the cloned repo, and run:<br>
<code>$ git submodule update --init</code></li>

<li>Make sure that the project is in a case insensitive folder. This is required because of <a href="https://github.com/AlexDarigan/WAT-GDScript/issues/195" target="_blank" hreflang="en">a bug with WAT</a>. <a href="https://github.com/AlexDarigan/WAT-GDScript/commit/40431c5be2f349376379da75287e92e53cd519cb" target="_blank" hreflang="en">That bug has already been fixed</a>, but <a href="https://github.com/AlexDarigan/WAT-GDScript/releases/tag/v5.0.0Release" target="_blank" hreflang="en">the latest stable version of WAT</a> doesn't include the fix yet. If you're running Linux on a case sensitive filesystem, then you may be able to do the following to create a case insensitive folder (<a href="https://unix.stackexchange.com/a/559022" target="_blank" hreflang="en">thanks, A.B</a>):

<!--This marks the start of a section that is derived from A.B's answer and is made available under the terms of the CC-BY-SA-4.0 license.-->
<ol type="A">
<li>Make that you meet the system requirements:

<ul>
<li>Linux >= 5.2. You can check this by running <code>$ uname -a</code>.</li>
<li>e2fsprogs >= 1.45. You can check this by running <code>$ mkfs.ext4 -V</code>.</li>
</ul>

</li>

<li><code>cd</code> to a directory outside of the repo.</li>

<li>Create a new filesystem. To do this,
<ol type="I">

<li>Create a file that's large enough to contain the repo, any changes you plan on making, and any files that Godot will automatically generate. To do this, run:<br>
<code>$ truncate -s 2GiB filesystem.raw</code></li>
<li>Format the file as an EXT4 filesystem. Make sure to enable the casefold option. To do this, run:<br>
<code>$ mkfs.ext4 -O casefold filesystem.raw</code></li>
<li>(Optional) Make root the owner of the file. To do this, run:<br>
<code># chown root:root filesystem.raw</code></li>
<li>(Optional) Make sure that only the owner of the file can read and write to it directly. To do this, run:<br>
<code># chmod 600 filesystem.raw</code></li>

</ol></li>

<li>Create a mount point. To do this, run:<br>
<code>$ mkdir mount_point</code></li>
<li>Mount the filesystem. To do this, run:<br>
<code># mount -o loop filesystem.raw mount_point</code></li>
<li>Change the owner of the mount point. To do this, run:<br>
<code># chown &lt;username&gt;:&lt;username&gt; mount_point</code></li>
<li>Inside the mount point, create a directory with the casefold attribute. To do this, run:<br>
<code>$ mkdir mount_point/case_insensitive</code><br>
<code>$ chattr +F mount_point/case_insensitive</code></li>
<li>Copy the repo into the <code>case_insensitive</code> folder.</li>
<!--This marks the end of the section that is made available under the terms of the CC-BY-SA-4.0 license.-->

<li>Open Godot and import the <code>ini_reuse_commenter_tests/project</code> folder.</li>
</ol>

</ol>

## Pre-commit
If you're going to contribute to this project, then you may want to have your contributions automatically checked. To do so,

1. Make sure that [pre-commit](https://pre-commit.com/) is installed.
1. `cd` to the root of the repo.
1. Run `$ pre-commit install`.
