# SPDX-FileCopyrightText: 2021 Jason Yundt <swagfortress@gmail.com>
# SPDX-License-Identifier: CC0-1.0

extends WAT.Test


const Rule = preload("res://addons/ini_reuse_commenter/rule.gd")
const PATTERNS = [
		".*",
		"$res://foo\\.tscn^",
		"thirdparty/.*",
		"$res://art/player_.*_[0-9]+\\.webp\\.import^",
		".*\\.import",
		"[a-c][1-3]",
		"$res://project\\.godot^",
]
const FILE_COPYRIGHT_TEXTS_LIST = [
		["asdfasdfasdf"],
		[
				"2020 John Doe <jdoe@example.org>",
				"2021 Jane Doe <jdoe2@example.org>"
		],
		["2021 Jason Yundt <swagfortress@gmail.com>"],
		["1998-1999 John Doe", "2000-2004 Jane Doe"],
		["2"]
]
const LICENSE_IDENTIFIERS_LIST = [
		["CC0-1.0"],
		["GPL-3.0-only WITH Classpath-exception-2.0"],
		["MIT", "CC-BY-SA-4.0"]
]


func test_init():
	for pattern in PATTERNS:
		for file_copyright_texts in FILE_COPYRIGHT_TEXTS_LIST:
			for license_identifiers in LICENSE_IDENTIFIERS_LIST:
				for parse_as_config in [false, true]:
					var rule = Rule.new(
							pattern,
							file_copyright_texts,
							license_identifiers,
							parse_as_config
					)

					asserts.is_equal(
							pattern,
							rule.regex.get_pattern()
					)
					asserts.is_equal(
							file_copyright_texts,
							rule.file_copyright_texts
					)
					asserts.is_equal(
							license_identifiers,
							rule.license_identifiers
					)
					asserts.is_equal(
							parse_as_config,
							rule.parse_as_config
					)
